package example.isaac.animations;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

//    boolean bartIsShowing = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView bartImg = (ImageView) findViewById(R.id.imv_one);
        bartImg.setX(-1000);
        bartImg.animate().translationXBy(1000).setDuration(1000);

    }

    public void animateClick(View view) {
        ImageView bartImg = (ImageView) findViewById(R.id.imv_one);
        ImageView homerImg = (ImageView) findViewById(R.id.imv_two);

        // Fade
//        if (bartIsShowing) {
//            bartIsShowing = false;
//
//            bartImg.animate().alpha(0).setDuration(1000);
//            homerImg.animate().alpha(1).setDuration(1000);
//        } else {
//            bartIsShowing = true;
//
//            bartImg.animate().alpha(1).setDuration(1000);
//            homerImg.animate().alpha(0).setDuration(1000);
//        }

        // Translations
//        bartImg.animate().translationXBy(-2000).setDuration(1000);

        // Rotation
//        bartImg.animate().rotation(180).setDuration(1000);

        // Scale
//        bartImg.animate().scaleX(0.5f).scaleY(0.5f).setDuration(1000);


    }
}
